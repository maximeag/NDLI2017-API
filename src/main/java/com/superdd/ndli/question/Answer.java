package com.superdd.ndli.question;

import com.superdd.ndli.user.User;

import javax.persistence.*;

@Entity
public class Answer {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private Question question;

    @Column
    private boolean answer;

    @ManyToOne
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
