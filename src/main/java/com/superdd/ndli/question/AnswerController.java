package com.superdd.ndli.question;

import com.superdd.ndli.user.User;
import com.superdd.ndli.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/answers")
public class AnswerController {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(method = RequestMethod.POST)
    public Answer save(@RequestBody Map<String, Object> body) {

        String phone = (String) body.get("phone");
        long questionId = (int) body.get("questionId");
        boolean res = (boolean) body.get("answer");

        Question question = questionRepository.findOne(questionId);
        User user = userRepository.findUserByPhone(phone);

        Answer answer = new Answer();
        answer.setQuestion(question);
        answer.setUser(user);
        answer.setAnswer(res);

        return answerRepository.save(answer);
    }
}
