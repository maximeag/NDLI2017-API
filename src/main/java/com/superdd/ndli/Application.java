package com.superdd.ndli;

import com.superdd.ndli.question.Question;
import com.superdd.ndli.question.QuestionRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.ArrayList;
import java.util.List;

@EnableJpaRepositories
@SpringBootApplication
public class Application {

    @Autowired
    private QuestionRepository questionRepository;

    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }

    @Bean
    InitializingBean fixture() {

        return () -> {

            List<Question> questions = new ArrayList<>();

            questions.add(new Question("Êtes-vous blessé ?"));
            questions.add(new Question("Le lieu est-il sécurisé ?"));
            questions.add(new Question("La ou les victimes sont elles conscientes ?"));
            questions.add(new Question("La ou les victimes respirent-elles normalement ?"));

            questionRepository.save(questions);
        };
    }
}
