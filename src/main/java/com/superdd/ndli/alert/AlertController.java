package com.superdd.ndli.alert;

import com.superdd.ndli.user.User;
import com.superdd.ndli.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/alerts")
public class AlertController {

    @Autowired
    private AlertRepository alertRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Alert> findAll() {

        return alertRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Alert save(@RequestBody Map<String, Object> body) {

        String phone = (String) body.get("phone");

        User user = userRepository.findUserByPhone(phone);

        if (user == null) {

            user = new User();
            user.setPhone(phone);

            user = userRepository.save(user);
        }

        Alert alert = new Alert();

        alert.setUser(user);
        alert.setLatitude((double) body.get("latitude"));
        alert.setLongitude((double) body.get("longitude"));

        alert = alertRepository.save(alert);

        return alertRepository.save(alert);
    }
}
