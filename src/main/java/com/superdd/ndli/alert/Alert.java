package com.superdd.ndli.alert;

import com.superdd.ndli.user.User;

import javax.persistence.*;

@Entity
public class Alert {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private User user;

    @Column
    private double latitude;

    @Column
    private double longitude;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
